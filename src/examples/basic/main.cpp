#include "config.h"
#include "Sprocket.h"
#include "RcSwitchPlugin.h"

Sprocket *sprocket;

void setup()
{
    sprocket = new Sprocket({STARTUP_DELAY, SERIAL_BAUD_RATE});
    sprocket->addPlugin(new RcSwitchPlugin({D4}));
    sprocket->activate();
}

void loop()
{
    sprocket->loop();
    yield();
}