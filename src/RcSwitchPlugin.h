#ifndef __RC_SWITCH_PLUGIN__
#define __RC_SWITCH_PLUGIN__

#define _TASK_SLEEP_ON_IDLE_RUN
#define _TASK_STD_FUNCTION

#include <Plugin.h>
#include <RcSwitchConfig.h>
#include <RCSwitch.h>
#include "utils/print.h"

using namespace std;
using namespace std::placeholders;


class RcSwitchPlugin : public Plugin
{
  public:
    RCSwitch *rcSwitch;
    RcSwitchConfig config;
    RcSwitchPlugin(RcSwitchConfig cfg);
    void activate(Scheduler *scheduler);
    void switchOn(String msg);
    void switchOff(String msg);
};

#endif