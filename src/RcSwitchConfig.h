#ifndef __RC_SWITCH_CONFIG__
#define __RC_SWITCH_CONFIG__

#include <JsonStruct.h>
#include "utils/print.h"

using namespace std;
using namespace std::placeholders;

struct RcSwitchConfig
{
    int txPin;
};

struct RcSwitchPayload
{
    const char *group;
    const char *device;
};

struct RcSwitchPayloadJson : public RcSwitchPayload, public JsonStruct
{
    int verifyJsonObject(JsonObject &json)
    {
        return json.containsKey("group") 
            && json.containsKey("device") 
            && json.success();
    };
    void mapJsonObject(JsonObject &root)
    {
        root["group"] = group;
        root["device"] = device;
    }
    void fromJsonObject(JsonObject &json)
    {
        if (!verifyJsonObject(json))
        {
            Serial.println("ERROR: cannot parse JSON object");
            valid = 0;
            return;
        }
        group = getAttr(json, "group", group);
        device = getAttr(json, "device", device);
        valid = 1;
    };
};

#endif