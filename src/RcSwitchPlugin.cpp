#include "RcSwitchPlugin.h"

RcSwitchPlugin::RcSwitchPlugin(RcSwitchConfig cfg)
{
    config = cfg;
    rcSwitch = new RCSwitch();
}

void RcSwitchPlugin::activate(Scheduler *scheduler)
{
    rcSwitch->enableTransmit(config.txPin);
    subscribe("rf/switch/on", bind(&RcSwitchPlugin::switchOn, this, _1));
    subscribe("rf/switch/off", bind(&RcSwitchPlugin::switchOff, this, _1));
    PRINT_MSG(Serial, "RC", "plugin activated");
}

void RcSwitchPlugin::switchOn(String msg)
{
    Serial.println("Switch ON");
    RcSwitchPayloadJson payload;
    payload.fromJsonString(msg);
    if(payload.valid){
        rcSwitch->switchOn(payload.group, payload.device);
    }
}
void RcSwitchPlugin::switchOff(String msg)
{
    Serial.println("Switch OFF");
    RcSwitchPayloadJson payload;
    payload.fromJsonString(msg);
    if(payload.valid){
        rcSwitch->switchOff(payload.group, payload.device);
    }
}